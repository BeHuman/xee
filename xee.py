#!/usr/bin/python 
# -*- coding: utf8 -*- 

import sys, os, string
from datetime import datetime
try: import xmpp
except ImportError:
    print >> sys.stderr, """
    You need to install xmpppy from http://xmpppy.sf.net/.
    On Debian-based systems, install the python-xmpp package.
    apt-get install python-xmpp
    """
    sys.exit(-1)
try: import sqlite3
except ImportError:
    print >> sys.stderr, """
    On Debian-based systems, install the sqlite3 package.
    apt-get install sqlite3
    """
    sys.exit(-1)



intro="""XEE XMPP BOT 
GNU/GPL v3 
XEE is a bot for the XMPP protocol 
Create by Be Human (craft at ckdevelop.org)""" 

############################ bot logic start ##################################### 
def timestamp():
    timestamp=datetime.now()
    return str(timestamp.year)+("0"if len(str(timestamp.month))==1 else "")+str(timestamp.month)+("0"if len(str(timestamp.day))==1 else "")+str(timestamp.day)+("0"if len(str(timestamp.hour))==1 else "")+str(timestamp.hour)+("0"if len(str(timestamp.minute))==1 else "")+str(timestamp.minute)+("0"if len(str(timestamp.second))==1 else "")+str(timestamp.second)

def user_exist(user):
    try:
        REQ.execute("""SELECT * FROM events WHERE user=?""", (user,))
        rows = REQ.fetchall()
        BDD.commit()
        if len(rows)>0: return True
    except (sqlite3.OperationalError): return False
    return False

def event_type(user):
    try:
        REQ.execute("""SELECT * FROM events WHERE user=?""", (user,))
        events = REQ.fetchone()
        BDD.commit()
        return events[1]
    except (sqlite3.OperationalError): return ""
    return ""

def event_timestamp(user):
    try:
        REQ.execute("""SELECT * FROM events WHERE user=?""", (user,))
        events = REQ.fetchone()
        BDD.commit()
        return events[2]
    except (sqlite3.OperationalError): return ""
    return ""

def formule(text):
    try:
        if type(eval(text))==int or type(eval(text))==float or type(eval(text))==long:
            return str(eval(text))
        else:
            return ""
    except (NameError, SyntaxError, TypeError): 
        return ""

def procedure(script):
    try:
        exec script
        return script
    except (NameError, SyntaxError, TypeError):
        return False


def display_man():
    return u"""Enregistrer une nouvelle idée:
\t(idée)atttribut=valeur
\t(idée)atttribut!valeur
\t(idée)atttribut<valeur
\t(idée)atttribut>valeur

Interroger la base de donnée d'idée:
\t?idée
\t?:=valeur
\t?:=
\t?idée:attribut
\t?idée:attribut!valeur

Supprimer une idée de la base de donnée:
\t!idée
\t!:=valeur
\t!:=
\t!idée:attribut
\t!idée:attribut!valeur

Enregistrer un nouveau mot à un des dictionnaires.
\t<mot>type:genre:temps:desc
\t<manger>verbe:neutre:indicatif:se nourrir
\t<mange>manger:je:present
\t<manges>manger:tu:present
\t<mange>manger:il:present
\t<mange>manger:elle:present
\t<mangeons>manger:nous:present
\t<mangez>manger:vous:present
\t<mangent>manger:ils:present
\t<mangent>manger:elles:present
\t<comment>adverbe:?\t\t\t\tAjout d'un adverbe d'interrogation
\t<comment>adverbe:!\t\t\t\tAjout d'un adverbe d'affirmation

Interroger un ou plusieurs dictionnaires:
\t<?mot>\t\t\t\tinterroge le dictionnaire
\t<?mot>type:genre\tinterroge le dictionnaire avec filtre type et genre..etc

Supprimer un mot d'un dictionnaire:
\t<!mot>\t\t\t\tsupprime un mot du dictionnaire
\t<!mot>type:genre:temps:desc\tsupprime un mot du dictionnaire avec filtre type et genre ...etc

Injecter du code python
\t$print \"salut le monde\"

Executer du code python
\t>print \"salut le monde\"
"""

def getObjects(DICTIONARY, NAME, listMots, Z):
    if len(DICTIONARY)>0:
        try:
            REQ.execute("""SELECT * FROM synapse WHERE name=? AND attribute=?""", (NAME,listMots[Z+1],))
            objs = REQ.fetchall()
            if len(objs)<=0:
                REQ.execute("""SELECT * FROM synapse WHERE name=? AND attribute=?""", (NAME,listMots[Z-1],))
                objs = REQ.fetchall()
        except IndexError:
            try:
                REQ.execute("""SELECT * FROM synapse WHERE name=? AND attribute=?""", (NAME,listMots[Z-1],))
                objs = REQ.fetchall()
            except IndexError:
                objs=[]
    return objs

def updateEventsType(_type, user_id):
    REQ.execute("""UPDATE events SET type=?, timestamp=? WHERE user=?""", (_type, timestamp(),user_id,))
    BDD.commit()
    
def updateEventsActor(_actor, user_id):
    REQ.execute("""UPDATE events SET actor=?, timestamp=? WHERE user=?""", (_actor, timestamp(),user_id,))
    BDD.commit()
    
def updateEventsAction(_action, user_id):
    REQ.execute("""UPDATE events SET action=?, timestamp=? WHERE user=?""", (_action, timestamp(),user_id,))
    BDD.commit()
    
def updateEventsTemps(_temps, user_id):
    REQ.execute("""UPDATE events SET temps=?, timestamp=? WHERE user=?""", (_temps, timestamp(),user_id,))
    BDD.commit()
    
def getEventsType(user_id):
    REQ.execute("""SELECT type FROM events WHERE user=?""", (user_id,))
    EV = REQ.fetchone()
    if len(EV)>0:
        return EV[0]
        
def getEventsActor(user_id):
    REQ.execute("""SELECT actor FROM events WHERE user=?""", (user_id,))
    EV = REQ.fetchone()
    if len(EV)>0:
        return EV[0]
        
def getEventsAction(user_id):
    REQ.execute("""SELECT action FROM events WHERE user=?""", (user_id,))
    EV = REQ.fetchone()
    if len(EV)>0:
        return EV[0]

def getEventsTemps(user_id):
    REQ.execute("""SELECT temps FROM events WHERE user=?""", (user_id,))
    EV = REQ.fetchone()
    if len(EV)>0:
        return EV[0]

def getEventsOK(user_id):
    REQ.execute("""SELECT type, actor, action, temps FROM events WHERE user=?""", (user_id,))
    EV = REQ.fetchone()
    if len(EV)>0:
        if EV[0]=='' or EV[1]=='' or EV[2]=='' or EV[3]=='':return False
        else: return True
    else: return False

def getEventsContext(user_id):
    REQ.execute("""SELECT type, actor, action, temps FROM events WHERE user=?""", (user_id,))
    EV = REQ.fetchone()
    if len(EV)>0:
        return EV[0]+' '+EV[1]+' '+EV[2]+' '+EV[3]

def clearEventsContext(user_id):
    REQ.execute("""UPDATE events SET type='', actor='', action='', temps='', timestamp=? WHERE user=?""", (timestamp(),user_id,))
    BDD.commit()

XMPP=None
def parser(conn,mess, retour):
    RETURN=False
    OUT=''
    ACTOR=''
    ACTION=''
    PREVIOUS_WORD=''
    is_question=False
    is_exclamation=False
    XMPP=mess
    func="";
    try:
        execfile("include.py")
    except (NameError, SyntaxError, TypeError, IOError) as e:
        pass
    try:
        message=mess.getBody() 
        if message==None:return
        message_origin=message

        message=message.replace("!"," ").replace("?"," ").replace("."," ").replace(","," ").replace(";"," ").replace("-"," ").replace("'"," ")
        print message
        nickname=mess.getFrom().getNode() 
        user_id=str(mess.getFrom())
        BDD_CUST=sqlite3.connect(user_id.replace('/','_').replace('\\','_')+'.db')
        CUST = BDD_CUST.cursor()
        
        listMots = message.split()
        context_global=''
        print len(listMots)
        print listMots
        if message_origin[len(message_origin)-1]=='?' and not is_question:
            is_question=True
            updateEventsType("?", user_id)
            print "is_question="+str(is_question)
                        
        if message_origin[len(message_origin)-1]=='!' and not is_exclamation:
            is_exclamation=True
            updateEventsType("!", user_id)
            print "is_exclamation="+str(is_exclamation)
        
        for Z in range(len(listMots)):
            REQ.execute("""SELECT * FROM dictionary WHERE name=?""", (listMots[Z],))
            DICTIONARY = REQ.fetchall()
            print listMots[Z]+' dico = '+str(DICTIONARY)
            if DICTIONARY!=None and len(DICTIONARY)<=0:
                ret="\n" if OUT!='' else ""
                OUT+=ret+u"Je ne connais pas le mot \""+listMots[Z]+u"\". Peux-tu me l\'apprendre?"
                RETURN=True
            
            for DICT in DICTIONARY:
                try: NAME=DICT[0] if DICT[0]!='' else ""
                except (IndexError, TypeError, NameError) as e:NAME=listMots[Z]
                try: TYPE=DICT[1] if DICT[1]!='' else ""
                except (IndexError, TypeError, NameError) as e:TYPE=''
                try: GENRE=DICT[2] if DICT[2]!='' else ""
                except (IndexError, TypeError, NameError) as e:GENRE=''
                try: TEMPS=DICT[3] if DICT[3]!='' else ""
                except (IndexError, TypeError, NameError) as e:TEMPS=''
                try: DESC=DICT[4] if DICT[4]!='' else ""
                except (IndexError, TypeError, NameError) as e:DESC=''
                    
                if TYPE!='verbe' and TYPE!='ADV' and TYPE!='NOM' and TYPE!='ADJ' and TYPE!='cod' and TYPE!='attribut' and TYPE!="PRO-per" and TYPE!="PRO-rel" and TYPE!="PRO-int" and TYPE!="PRO-dem" and TYPE!="ONO" and TYPE!="ART-ind" and TYPE!="PRO-ind":
                            
                    REQ.execute("""SELECT * FROM dictionary WHERE name=? AND (type='verbe' OR type='ADV' OR type='NOM' OR type='ADJ' OR type='cod' OR type='attribut' OR type='PRO-per' OR type='PRO-dem' OR type='RO-rel' OR type='PRO-int' OR type='ONO' OR type='ART-ind' OR type='PRO-ind')""", (TYPE,))
                    DICTIONARY2 = REQ.fetchone()
                    if DICTIONARY2!=None and len(DICTIONARY2)>0 :
                        try:
                            TYPE=DICTIONARY2[1]
                            NAME=DICTIONARY2[0]
                        except (IndexError) as e: pass
                if '-int' in TYPE and not is_exclamation:
                    is_question=True
                    updateEventsType("?"+NAME, user_id)
                    print "is_question="+str(is_question)
                    
                if TYPE=='PRO-rel' and (is_question or '?' in getEventsType(user_id)):
                    is_question=True
                    updateEventsType("?"+NAME, user_id)
                    print "is_question="+str(is_question)
                    
                        
                if 'exclamation' in TYPE and not is_question:
                    is_exclamation=True
                    updateEventsType("!", user_id)
                    print "is_exclamation="+str(is_exclamation)

                if TYPE=='PRO-per':
                    ACTOR=NAME
                    updateEventsActor(ACTOR, user_id)
                    if ACTION!='' and not is_question:
                        updateEventsType('?', user_id)
                        is_question=True
                        print "is_question="+str(is_question)
                    print "ACTOR="+ACTOR
                    
                if TYPE=='PRO-dem':
                    ACTOR=NAME
                    updateEventsActor(ACTOR, user_id)
                    if ACTION!='' and not is_question:
                        updateEventsType('?', user_id)
                        is_question=True
                        print "is_question="+str(is_question)
                    print "ACTOR="+ACTOR

                if TYPE=='verbe':
                    ACTION=NAME
                    updateEventsAction(ACTION, user_id)
                    updateEventsTemps(TEMPS, user_id)
                    print "ACTION="+ACTION

        print getEventsContext(user_id)
        if getEventsOK(user_id):
            REQ.execute("""SELECT value FROM synapse WHERE name=? AND attribute='return' AND operator='='""", (getEventsContext(user_id),))
            return_ = REQ.fetchone()
            if return_!=None and len(return_)>0: 
                func="OUT+=u\""+return_[0]+" \"\n"
                try: exec func
                except (NameError, SyntaxError, TypeError) as e: print e
            REQ.execute("""SELECT value FROM synapse WHERE name=? AND attribute='def' AND operator='='""", (getEventsContext(user_id),))
            def_ = REQ.fetchone()
            if def_!=None and len(def_)>0:
                try: exec def_[0]
                except (NameError, SyntaxError, TypeError) as e: print e
            REQ.execute("""SELECT value FROM synapse WHERE name=? AND attribute='exec' AND operator='='""", (getEventsContext(user_id),))
            exec_ = REQ.fetchone()
            if exec_!=None and len(exec_)>0:
                REQ.execute("""SELECT value FROM synapse WHERE name=? AND attribute='params' AND operator='='""", (getEventsContext(user_id),))
                params_ = REQ.fetchone()
                if params_!=None and len(params_)>0: func="OUT+="+exec_[0]+"("+params_[0]+")\n"
                else: func="OUT+="+exec_[0]+"()\n"
                try: exec func
                except (NameError, SyntaxError, TypeError) as e: print e
            print OUT
            clearEventsContext(user_id)
    except (NameError, AttributeError) as e:
        print e
    
    if OUT!='': return OUT
    else: return ''

retour=''
def messageCB(conn,mess): 
    text=mess.getBody() 
    nickname=mess.getFrom().getNode() 
    user_id=str(mess.getFrom())
    if text==None:
        text='**event**'
    status=""
    bbjr=False 
    barv=False 
    retour=''
    try:
        if user_exist(user_id)==False:
            REQ.execute("""INSERT INTO events(user,  type, timestamp) VALUES(?, ?, ?)""", (user_id, "hello", timestamp(),))
            BDD.commit()
            retour=u"Enchanté "+nickname+u"\nJe suis Xee. Tu peux parler avec moi, cependant je ne suis pas encore très intuitif."
        
        BDD_CUST=sqlite3.connect(user_id.replace('/','_').replace('\\','_')+'.db')
        CUST = BDD_CUST.cursor()
        CUST.execute("""
CREATE TABLE IF NOT EXISTS `history` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `message`	TEXT NOT NULL,
    `response`	TEXT NOT NULL,
    `timestamp`	TEXT NOT NULL DEFAULT 0
)
""")
        CUST.execute("""
CREATE TABLE IF NOT EXISTS `objects`(
    `name`      TEXT NOT NULL,
    `attribute` TEXT NOT NULL DEFAULT '',
    `operator`  TEXT NOT NULL DEFAULT '',
    `value`     TEXT NOT NULL DEFAULT ''
)
""")        
        BDD_CUST.commit()
        BDD_CUST.close()
        
        if text=="help" or text=="aide": retour=display_man()
        iteration=0
        ncaract=0
        dico_ref=False
        dico_delete=False
        dico_question=False
        dico_name=''
        dico_type=''
        dico_genre=''
        dico_temps=''
        dico_desc=''
        object_ref=False
        object_question=False
        object_delete=False
        object_name=''
        object_attribute=''
        object_operator=''
        object_value=''
        as_attribute=False
        add_script=False
        exec_script=False
        PARSER=''
        _script=''
        formule_result=formule(text)
        if formule_result!="":
            conn.send(xmpp.Message(mess.getFrom(),formule_result))
            BDD_CUST=sqlite3.connect(user_id.replace('/','_').replace('\\','_')+'.db')
            CUST = BDD_CUST.cursor()
            CUST.execute("""INSERT INTO history( message,response, timestamp) VALUES(?, ?, ?)""", ( text, str(formule_result), timestamp(), ))
            BDD_CUST.commit()
            BDD_CUST.close()
            return
        
        for word in text:
            if add_script:
                _script+=word
            elif iteration == 0 and ncaract==0:
                if word == "(": 
                    object_ref=True
                    iteration=1
                elif word == "<": 
                    dico_ref=True
                    iteration=1
                elif word == "?": 
                    object_question=True
                    iteration=1
                elif word == "!": 
                    object_delete=True
                    iteration=1
                elif word == "$": 
                    add_script=True
                elif word == ">": 
                    add_script=True
                    exec_script=True
            elif iteration==1:
                if (word==')' and object_ref) or (word=='>' and dico_ref) or (word==':' and (object_delete or object_question)): iteration=2
                elif dico_ref and ncaract==1 and word=='!': dico_delete=True
                elif dico_ref and ncaract==1 and word=='?': dico_question=True
                elif dico_ref: dico_name+=word
                else: object_name+=word
            elif iteration==2:
                if word==':' and dico_ref:
                    iteration=3
                elif (word=='=' or word=='!' or word=='<' or word=='>') and (object_ref or object_question or object_delete):
                    object_operator=word 
                    iteration=3
                elif dico_ref: dico_type+=word
                else: object_attribute+=word
            elif iteration==3:
                if word==':' and dico_ref:iteration=4
                elif dico_ref: dico_genre+=word
                else: object_value+=word
            elif iteration==4:
                if word==':' and dico_ref: iteration=5
                if dico_ref: dico_temps+=word
            elif iteration==4:
                if dico_ref: dico_desc+=word
            ncaract+=1
        
        ret="\n" if retour!='' else ""
        if add_script:
            try:
                if not exec_script:
                    f = open("include.py", 'a')
                    f.write("\n"+_script)
                    f.close()
                try:
                    execfile("include.py")
                except (NameError, SyntaxError, TypeError, IOError) as e:
                    pass
                if exec_script:
                    exec _script
                BDD_CUST=sqlite3.connect(user_id.replace('/','_').replace('\\','_')+'.db')
                CUST = BDD_CUST.cursor()
                CUST.execute("""INSERT INTO history(message,response, timestamp) VALUES(?, ?, ?)""", (text, "exec \"\"\""+_script+"\"\"\"", timestamp(), ))
                BDD_CUST.commit()
                BDD_CUST.close()
            except (NameError, SyntaxError, TypeError) as e:
                print e
            return
        elif object_ref: # (name)attribute=value
            ''' INSERT IDÉE '''
            try:
                REQ.execute("""INSERT INTO synapse(name,  attribute,operator, value) VALUES(?, ?, ?, ?)""", (object_name, object_attribute, object_operator, object_value))

                BDD.commit()
                retour=ret+u"Information sauvegardé."
            except (sqlite3.OperationalError):
                retour=ret+u"Erreur lors de la sauvegarde de l'information."
                pass
        elif dico_ref: # (name)attribute=value
            if dico_delete:
                ''' SUPRESSION MOT DICTIONNAIRE '''
                try:
                    var_=[]
                    n_="name=? " if dico_name!='' else ""
                    and_="AND " if dico_name!='' else ""
                    if dico_name!='': var_.append(dico_name)
                    
                    t_=and_+"type=? " if dico_type!='' else ""
                    and_="AND " if dico_type!='' else ""
                    if dico_type!='': var_.append(dico_type)
                    
                    g_=and_+"genre=? " if dico_genre!='' else ""
                    and_="AND " if dico_genre!='' else ""
                    if dico_genre!='': var_.append(dico_genre)
                    
                    tt_=and_+"temps=? " if dico_temps!='' else ""
                    and_="AND " if dico_temps!='' else ""
                    if dico_temps!='': var_.append(dico_temps)
                    
                    d_=and_+"desc=? " if dico_desc!='' else ""
                    if dico_desc!='': var_.append(dico_desc)
                    
                    REQ.execute("""DELETE from dictionary WHERE """+n_+t_+g_+tt_+d_, var_ )
                    BDD.commit()
                    retour=ret+u"Mot supprimé du dictionnaire."
                    
                except (sqlite3.OperationalError):
                    retour=ret+u"Erreur lors de la supression du mot dans le dictionnaire."
                    pass
            elif dico_question:
                ''' QUESTION DICTIONNAIRE '''
                var_=[]
                n_="name=? " if dico_name!='' else ""
                and_="AND " if dico_name!='' else ""
                if dico_name!='': var_.append(dico_name)
                
                t_=and_+"type=? " if dico_type!='' else ""
                and_="AND " if dico_type!='' else ""
                if dico_type!='': var_.append(dico_type)
                
                g_=and_+"genre=? " if dico_genre!='' else ""
                and_="AND " if dico_genre!='' else ""
                if dico_genre!='': var_.append(dico_genre)
                
                tt_=and_+"temps=? " if dico_temps!='' else ""
                and_="AND " if dico_temps!='' else ""
                if dico_temps!='': var_.append(dico_temps)
                
                d_=and_+"desc=? " if dico_desc!='' else ""
                if dico_desc!='': var_.append(dico_desc)

                REQ.execute("""SELECT * FROM dictionary WHERE """+n_+t_+g_+tt_+d_, var_)
                rows = REQ.fetchall()
                BDD.commit()
                if len(rows)>0:
                    for row in rows:
                        t=row[1]if row[1]!=None and row[1]!="" else ""
                        g=row[2]if row[2]!=None and row[2]!="" else ""
                        tt=row[3]if row[3]!=None and row[3]!="" else ""
                        d=row[4]if row[4]!=None and row[4]!="" else ""
                        
                        ret="\n" if retour!='' else "" 
                        T_=u"est de type '"+t+"'" if t!='' else ""
                        and_=u" et " if t!='' else ""
                        G_=and_+u"il est de genre '"+g+"'" if g!='' else ""
                        and_=u" et " if t!='' or g!='' else ""
                        TT_=and_+u"à '"+tt+"'" if tt!='' else ""
                        
                        and_=u" : " if t!='' or g!='' or tt!='' else ""
                        D_=and_+u"\n\t'"+d+"'" if d!='' else ""
                        retour+=ret+u"Le mot '"+row[0]+"' "+T_+G_+TT_+D_

            else:
                ''' INSERT MOT DICTIONNAIRE '''
                try:
                    var_=[]
                    n_="? " if dico_name!='' else ""
                    N_="name " if dico_name!='' else ""
                    and_=", " if dico_name!='' else ""
                    if dico_name!='': var_.append(dico_name)
                    
                    t_=and_+"? " if dico_type!='' else ""
                    T_=and_+"type " if dico_type!='' else ""
                    and_=", " if dico_type!='' else ""
                    if dico_type!='': var_.append(dico_type)
                    
                    g_=and_+"? " if dico_genre!='' else ""
                    G_=and_+"genre " if dico_genre!='' else ""
                    and_=", " if dico_genre!='' else ""
                    if dico_genre!='': var_.append(dico_genre)
                    
                    tt_=and_+"? " if dico_temps!='' else ""
                    TT_=and_+"temps " if dico_temps!='' else ""
                    and_=", " if dico_temps!='' else ""
                    if dico_temps!='': var_.append(dico_temps)
                    
                    d_=and_+"? " if dico_desc!='' else ""
                    D_=and_+"desc " if dico_desc!='' else ""
                    if dico_desc!='': var_.append(dico_desc)

                    REQ.execute("""INSERT INTO dictionary("""+N_+T_+G_+TT_+D_+""") VALUES("""+n_+t_+g_+tt_+d_+""")""", var_)
                    BDD.commit()
                    
                    retour=ret+u"Information sauvegardé dans le dictionnaire."
                except (sqlite3.OperationalError):
                    retour=ret+u"Erreur lors de la sauvegarde de l'information dans le dictionnaire."
                    pass
        elif object_delete:
            ''' SUPRESSION IDÉE '''
            try:
                var_=[]
                n_="name=? " if object_name!='' else ""
                and_="AND " if object_name!='' else ""
                if object_name!='': var_.append(object_name)
                
                a_=and_+"attribute=? " if object_attribute!='' else ""
                and_="AND " if object_attribute!='' else ""
                if object_attribute!='': var_.append(object_attribute)
                
                o_=and_+"operator=? " if object_operator!='' else ""
                and_="AND " if object_operator!='' else ""
                if object_operator!='': var_.append(object_operator)
                
                v_=and_+"value=? " if object_value!='' else ""
                if object_value!='': var_.append(object_value)
                
                REQ.execute("""DELETE from synapse WHERE """+n_+a_+o_+v_, var_ )
                BDD.commit()
                retour=ret+u"Information supprimé."
            except (sqlite3.OperationalError):
                retour=ret+u"Erreur lors de la supression de l'information."
                pass
        elif object_question:
            ''' QUESTION IDÉE'''
            retour=""
            
            #Recherche dans le nom
            try:
                var_=[]
                n_="name=? " if object_name!='' else ""
                and_="AND " if object_name!='' else ""
                if object_name!='': var_.append(object_name)
                
                a_=and_+"attribute=? " if object_attribute!='' else ""
                and_="AND " if object_attribute!='' else ""
                if object_attribute!='': var_.append(object_attribute)
                
                o_=and_+"operator=? " if object_operator!='' else ""
                and_="AND " if object_operator!='' else ""
                if object_operator!='': var_.append(object_operator)
                
                v_=and_+"value=? " if object_value!='' else ""
                if object_value!='': var_.append(object_value)
                
                REQ.execute("""SELECT * FROM synapse WHERE """+n_+a_+o_+v_, var_)
                rows = REQ.fetchall()
                BDD.commit()
                if object_operator!='':
                    if len(rows)>0:
                        for row in rows:
                            ret="\n" if retour!='' else ""
                            if object_operator=="!":
                                retour+=ret+u"Oui, "+row[1]+" de "+row[0]+u" n'est pas "+row[3]
                            elif object_operator=="=":
                                retour+=ret+u"Oui, "+row[1]+" de "+row[0]+u" est "+row[3]
                            elif object_operator==">":
                                retour+=ret+u"Oui, "+row[1]+" de "+row[0]+u" est plus grand que "+row[3]
                            elif object_operator=="<":
                                retour+=ret+u"Oui, "+row[1]+" de "+row[0]+u" est plus petit que "+row[3]
                    else:
                        ret="\n" if retour!='' else ""
                        retour+=ret+u"Non, je ne crois pas.\nJe n'ai aucune information en mémoire pour le moment :-("
              
                else:
                    for row in rows:
                        ret="\n" if retour!='' else ""
                        if row[2]=="!":
                            retour+=ret+row[1]+u" de "+row[0]+u" n'est pas "+row[3]
                        elif row[2]=="=":
                            retour+=ret+row[1]+u" de "+row[0]+u" est "+row[3]
                        elif row[2]==">":
                            retour+=ret+row[1]+u" de "+row[0]+u" est plus grand que "+row[3]
                        elif row[2]=="<":
                            retour+=ret+row[1]+u" de "+row[0]+u" est plus petit que "+row[3]
                        else:
                            retour+=ret+row[1]+u" de "+row[0]+u" est peut-être "+row[3]
            except (sqlite3.OperationalError):
                print "erreur"

        if not dico_ref and not object_ref and not object_question and not object_delete:
            PARSER=parser(conn,mess, retour)
            ret="\n" if retour!='' else ""
            retour+=ret+PARSER

        ''' FIN DE REQUETE '''
        if retour=='' and text!='**event**': retour=u"Désolé %s, je ne suis qu'un petit bot sans prétention." % nickname
        conn.send(xmpp.Message(mess.getFrom(),retour)) 
        BDD_CUST=sqlite3.connect(user_id.replace('/','_').replace('\\','_')+'.db')
        CUST = BDD_CUST.cursor()
        CUST.execute("""INSERT INTO history(message,response, timestamp) VALUES(?, ?, ?)""", (text, retour, timestamp(), ))
        BDD_CUST.commit()
        BDD_CUST.close()
    except (TypeError, UnicodeEncodeError):
        pass

def presenceCB(conn,pres):
    if pres.getType() == 'subscribe':
        conn.send(xmpp.Presence(to=pres.getFrom(), typ='subscribed'))
    text=pres.getShow()
    try:
        nickname=pres.getFrom().getNode() 
        user_id=str(pres.getFrom())
        BDD_CUST=sqlite3.connect(user_id.replace('/','_').replace('\\','_')+'.db')
        CUST = BDD_CUST.cursor()
        CUST.execute("""INSERT INTO history( message,response, timestamp) VALUES(?, ?, ?)""", ("STATUS "+text, "", timestamp(), ))
        BDD_CUST.commit()
        BDD_CUST.close()
    except (TypeError, UnicodeEncodeError):
        pass
############################# bot logic stop ##################################### 

def StepOn(conn): 
    try: 
        conn.Process(1) 
    except KeyboardInterrupt: 
        return 0 
    return 1 

def GoOn(conn): 
    while StepOn(conn): pass 


BotName="Xee"
jid=xmpp.JID("xee@isaac.asimov")
user,server,password=jid.getNode(),jid.getDomain(),"asimov"
conn=xmpp.Client(server,debug=[])
conres=conn.connect()

''' Gestion Base de donnée '''
BDD = sqlite3.connect('xee.db') # ('xee%s.db' %datetime.now())
REQ = BDD.cursor()

BDD_CUST=None
CUST=None

REQ.execute("""
CREATE TABLE IF NOT EXISTS `synapse`(
    `name`      TEXT NOT NULL,
    `attribute` TEXT NOT NULL DEFAULT '',
    `operator`  TEXT NOT NULL DEFAULT '',
    `value`     TEXT NOT NULL DEFAULT ''
)
""")
REQ.execute("""
CREATE TABLE IF NOT EXISTS `dictionary`(
    `name`      TEXT NOT NULL,
    `type`      TEXT NOT NULL DEFAULT '',
    `genre`     TEXT NOT NULL DEFAULT '',
    `temps`     TEXT NOT NULL DEFAULT '',
    `desc`      TEXT NOT NULL DEFAULT ''
)
""")
REQ.execute("""
CREATE TABLE IF NOT EXISTS `events` (
    `user`              TEXT NOT NULL UNIQUE,
    `type`              TEXT NOT NULL DEFAULT '',
    `actor`             TEXT NOT NULL DEFAULT '',
    `action`            TEXT NOT NULL DEFAULT '',
    `temps`             TEXT NOT NULL DEFAULT '',
    `timestamp`         TEXT NOT NULL DEFAULT 0,
    PRIMARY KEY(user)
)
""")
BDD.commit()

if not conres: 
    print "Unable to connect to server %s!"%server 
    sys.exit(1) 
if conres<>'tls': 
    print "Warning: unable to estabilish secure connection - TLS failed!" 
authres=conn.auth(user,password) 
if not authres: 
    print "Unable to authorize on %s - check login/password."%server 
    sys.exit(1) 
if authres<>'sasl': 
    print "Warning: unable to perform SASL auth os %s. Old authentication method used!"%server 
conn.RegisterHandler('message',messageCB) 
conn.RegisterHandler('presence',presenceCB) 
conn.sendInitPresence() 

if len(sys.argv)>2 and sys.argv[1]=='-m':
    conn.send(xmpp.Message("xee@isaac.asimov",sys.argv[2] ))
    print "Mode automatique"
GoOn(conn)
