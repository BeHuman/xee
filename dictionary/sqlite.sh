#! /bin/bash

inf=''
cat dictionary.csv | sed 's/\t/%/g' | while read line; do
    mot=`echo $line | cut -d'%' -f1`
    lemme=`echo $line | cut -d'%' -f3`
    type=`echo $line | cut -d'%' -f4`
    genre=`echo $line | cut -d'%' -f5`
    temps=`echo $line | cut -d'%' -f11`
    
    if [[ "$type" == 'VER' ]]; then
        type='verbe'
        echo $temps| sed 's/;/\n/g' | while read time; do
            if [[ $time == 'inf' ]]; then
                echo "INSERT INTO dictionary(name,type,genre,temps) VALUES('${mot}','${type}','${genre}','${time}');"
            else
                if [[ -f pers ]]; then rm pers; fi
                if [[ -f tim ]]; then rm tim; fi
                g=''
                t=''
                echo $time| sed 's/:/\n/g' | while read pers; do
                    if [[ $pers == '1s' ]]; then
                        echo 'je' > pers
                    elif [[ $pers == '2s' ]]; then
                        echo 'tu'> pers
                    elif [[ $pers == '3s' ]]; then
                        if [[ $genre == 'm' ]]; then
                            echo 'il'> pers
                        elif [[ $genre == 'f' ]]; then
                            echo 'elle'> pers
                        else
                            echo 'il;elle'> pers
                        fi
                    elif [[ $pers == '1p' ]]; then
                        echo 'nous'> pers
                    elif [[ $pers == '2p' ]]; then
                        echo 'vous'> pers
                    elif [[ $pers == '3p' ]]; then
                        if [[ $genre == 'm' ]]; then
                            echo 'ils'> pers
                        elif [[ $genre == 'f' ]]; then
                            echo 'elles'> pers
                        else
                            echo 'ils;elles'> pers
                        fi
                    else
                        echo "${t}${pers}"  >> tim
                    fi
                done
                if [[ -f pers ]]; then g=`cat pers`; fi
                if [[ -f tim ]]; then t=`cat tim | tr '\n' '-' | sed 's/-$//'`; fi
                if [[ $t == '-' ]]; then t=''; fi
                
                if [[ $g == 'il;elle' ]]; then
                    echo "INSERT INTO dictionary(name,type,genre,temps) VALUES('${mot}','${lemme}','il','${t}');"
                    echo "INSERT INTO dictionary(name,type,genre,temps) VALUES('${mot}','${lemme}','elle','${t}');"
                elif [[ $g == 'ils;elles' ]]; then
                    echo "INSERT INTO dictionary(name,type,genre,temps) VALUES('${mot}','${lemme}','ils','${t}');"
                    echo "INSERT INTO dictionary(name,type,genre,temps) VALUES('${mot}','${lemme}','elles','${t}');"
                else
                    echo "INSERT INTO dictionary(name,type,genre,temps) VALUES('${mot}','${lemme}','${g}','${t}');"
                fi
                
                if [[ -f pers ]]; then rm pers; fi
                if [[ -f tim ]]; then rm tim; fi
            fi
        done
    fi
    
done

