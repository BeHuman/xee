#! /bin/bash

inf=''
if [[ ! -d tmp ]]; then mkdir tmp; fi
if [[ -f tmp/pers ]]; then rm tmp/pers; fi
if [[ -f tmp/tim ]]; then rm tmp/tim; fi
cat dictionary.csv | grep -e 'VER' -e 'ind:fut' | sed 's/\t/%/g' | while read line; do
    mot=`echo $line | cut -d'%' -f1`
    lemme=`echo $line | cut -d'%' -f3`
    type=`echo $line | cut -d'%' -f4`
    genre=`echo $line | cut -d'%' -f5`
    temps=`echo $line | cut -d'%' -f11`
    
    if [[ "$type" == 'VER' ]]; then
        type='verbe'
        echo $temps| sed 's/;/\n/g' | while read time; do
            if [[ $time == 'inf' ]]; then
                m=`echo $mot | sed "s/'/@/g"`
                echo "INSERT OR IGNORE INTO dictionary(name,type,genre,temps) VALUES('${m}','${type}','${genre}','${time}');" >> tmp.sql
            else
                g=''
                t=''
                echo $time| sed 's/:/\n/g' | while read pers; do
                    if [[ $pers == '1s' ]]; then
                        echo 'je' > tmp/pers
                    elif [[ $pers == '2s' ]]; then
                        echo 'tu'> tmp/pers
                    elif [[ $pers == '3s' ]]; then
                        if [[ $genre == 'm' ]]; then
                            echo 'il'> tmp/pers
                        elif [[ $genre == 'f' ]]; then
                            echo 'elle'> tmp/pers
                        else
                            echo 'il;elle'> tmp/pers
                        fi
                    elif [[ $pers == '1p' ]]; then
                        echo 'nous'> tmp/pers
                    elif [[ $pers == '2p' ]]; then
                        echo 'vous'> tmp/pers
                    elif [[ $pers == '3p' ]]; then
                        if [[ $genre == 'm' ]]; then
                            echo 'ils'> tmp/pers
                        elif [[ $genre == 'f' ]]; then
                            echo 'elles'> tmp/pers
                        else
                            echo 'ils;elles'> tmp/pers
                        fi
                    else
                        echo "${t}${pers}"  >> tmp/tim
                    fi
                done
                if [[ -f tmp/pers ]]; then g=`cat tmp/pers`; fi
                if [[ -f tmp/tim ]]; then t=`cat tmp/tim | tr '\n' '-' | sed 's/-$//'`; fi
                if [[ -f tmp/pers ]]; then rm tmp/pers; fi
                if [[ -f tmp/tim ]]; then rm tmp/tim; fi
                if [[ "$t" == 'ind-fut' ]]; then
                    m=`echo $mot | sed "s/'/@/g"`
                    l=`echo $lemme | sed "s/'/@/g"`
                    if [[ $g == 'il;elle' ]]; then
                        echo "INSERT OR IGNORE  INTO dictionary(name,type,genre,temps) VALUES('${m}','${l}','il','${t}');" >> tmp.sql
                        echo "INSERT OR IGNORE  INTO dictionary(name,type,genre,temps) VALUES('${m}','${l}','elle','${t}');" >> tmp.sql
                    elif [[ $g == 'ils;elles' ]]; then
                        echo "INSERT OR IGNORE  INTO dictionary(name,type,genre,temps) VALUES('${m}','${l}','ils','${t}');" >> tmp.sql
                        echo "INSERT OR IGNORE  INTO dictionary(name,type,genre,temps) VALUES('${m}','${l}','elles','${t}');" >> tmp.sql
                    else
                        echo "INSERT OR IGNORE  INTO dictionary(name,type,genre,temps) VALUES('${m}','${l}','${g}','${t}');" >> tmp.sql
                    fi
                    break 
                fi
            fi
        done
    fi
    
done
if [[ -d tmp ]]; then rm -rf tmp; fi
cat tmp.sql | grep "'ind-fut'" | uniq > ind-fut.sql
rm tmp.sql

