#! /bin/bash

#conversion du csv en sql

if [[ -f dictionary.sql ]]; then rm dictionary.sql; fi
if [[ -f tmp.sql ]]; then rm tmp.sql; fi
cat dictionary.csv | grep -v 'VER' | sed 's/\t/%/g' | while read line; do
    mot=`echo $line | cut -d'%' -f1`
    lemme=`echo $line | cut -d'%' -f3`
    type=`echo $line | cut -d'%' -f4`
    genre=`echo $line | cut -d'%' -f5`
    m=`echo $mot | sed "s/'/@/g" | sed "s/ /_/g"`
    t=`echo $type | sed "s/:/-/g"`
    echo "INSERT OR IGNORE INTO dictionary(name,type,genre) VALUES('${m}','${t}','${genre}');" >> tmp.sql
    
done
cat tmp.sql | uniq > dictionary.sql
if [[ -f tmp.sql ]]; then rm tmp.sql; fi

